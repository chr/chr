Source: chr
Maintainer: Christoph Hueffelmann <chr@istoph.de>
Uploaders: Martin Hostettler <textshell@uchuujin.de>, Alex Myczko <tar@debian.org>
Section: editors
Priority: optional
Standards-Version: 4.7.0
Rules-Requires-Root: no
Build-Depends:
 debhelper-compat (= 13),
 cmake,
 meson,
 ninja-build,
 pkgconf,
 qt5-qmake,
 qttools5-dev-tools,
 qtbase5-dev,
 catch2,
 libtermpaint-dev,
 libposixsignalmanager-dev,
 libtuiwidgets-dev (>=0.2.1~),
 libkf5syntaxhighlighting-dev,
 libkf5syntaxhighlighting-tools
Homepage: https://github.com/istoph/editor
Vcs-Git: https://salsa.debian.org/debian/chr.git
Vcs-Browser: https://salsa.debian.org/debian/chr

Package: chr
Provides: editor
Depends: ${misc:Depends}, ${shlibs:Depends}
Architecture: any
Multi-Arch: foreign
Breaks: chr-tiny
Conflicts: chr-tiny
Replaces: chr-tiny
Description: terminal-based text editor
 It is designed to a be simple editor that is easy to use for users
 coming from desktop environments.
 .
 Keyboard shortcuts are similar to the default editors in GNOME, KDE
 and other desktop environments.
 .
 For example text is selected using shift+arrow keys, copy and paste
 uses ctrl+c and ctrl+v and there is a drop down menu(F10) to discover
 additional features and settings.
 .
 The look and feel is a blend of modern GUI editors and late 90s PC
 text mode editors (e.g. Turbo Vision based or edit.com) adapted to fit
 into terminal based workflows.
 .
 It implements many features like:
  - selecting text by holding Shift
  - usable without a complicated config file
  - block selection
  - multi windows (overlapping, tiled, fullscreen)
  - text from full width windows can be copied from the terminal without
    window borders, scrollbars or additional spaces interfering.
  - syntax highlighting
  - undo/redo
  - display line numbers
  - soft-wrapping of long lines
  - interactive search and replace (with regular expression support)
  - go-to line (and column) command
  - support stdin buffers
  - drop down menus

Package: chr-tiny
Provides: editor
Depends: ${misc:Depends}, ${shlibs:Depends}
Architecture: any
Multi-Arch: foreign
Description: terminal-based text editor - without syntax highlighting
 It is designed to a be simple editor that is easy to use for users
 coming from desktop environments.
 .
 Keyboard shortcuts are similar to the default editors in GNOME, KDE
 and other desktop environments.
 .
 For example text is selected using shift+arrow keys, copy and paste
 uses ctrl+c and ctrl+v and there is a drop down menu(F10) to discover
 additional features and settings.
 .
 The look and feel is a blend of modern GUI editors and late 90s PC
 text mode editors (e.g. Turbo Vision based or edit.com) adapted to fit
 into terminal based workflows.
 .
 It implements many features like:
  - selecting text by holding Shift
  - usable without a complicated config file
  - block selection
  - multi windows (overlapping, tiled, fullscreen)
  - text from full width windows can be copied from the terminal without
    window borders, scrollbars or additional spaces interfering.
  - undo/redo
  - display line numbers
  - soft-wrapping of long lines
  - interactive search and replace (with regular expression support)
  - go-to line (and column) command
  - support stdin buffers
  - drop down menus
 .
 The tiny version is compiled without support for syntax highlighting.
